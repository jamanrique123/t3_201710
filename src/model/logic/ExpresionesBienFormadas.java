package model.logic;

import model.data_structures.Stack;

public class ExpresionesBienFormadas {

	public static boolean expresionBienFormada(String expresion)
	{
		expresion = expresion.trim();
		Stack<Character> pila = new Stack<Character>();

		boolean estaBienFormada = true;

		int cantidadDeNumerosEnExpresion = 0;

		for (int i = 0; i< expresion.length(); i++)
		{
			char x = expresion.charAt(i);
			if (!esOperador(x) && !esParentesis(x)) //si no es parentesis ni operador, es n�mero
				cantidadDeNumerosEnExpresion++;
			pila.push(x);
		}

		System.out.println(pila);

		while (!pila.isEmpty())
		{
			char actual = pila.pop();
			int tamanoActual = pila.size();
			Stack<Character> auxiliar = pila;

			if (esParentesis(actual))
			{
				int tamanoProxParentesis = 0;
				boolean encontradoPareja = false;
				while (tamanoProxParentesis < tamanoActual && !encontradoPareja)
				{
					char actualAux = auxiliar.pop();
					if ( esParentesis(actualAux) && actualAux == parentesisContrario(actual))
					{
						encontradoPareja = true;
					}
					tamanoProxParentesis++;
				}

				if (cantidadDeNumerosEnExpresion%2 == 1 && tamanoProxParentesis != 0)
				{
					tamanoProxParentesis++;
				}
				
				if (tamanoProxParentesis % 2 != 1)
				{
					System.out.println(tamanoProxParentesis);
					estaBienFormada = false;
					break;
				}
			}
		}

		return estaBienFormada;
	}

	private static boolean esParentesis(char caracter)
	{
		boolean es = false;

		if (
				caracter == '(' ||
				caracter == ')' ||
				caracter == '[' ||
				caracter == ']'
				)
		{
			es = true;
		}

		return es;
	}

	private static char parentesisContrario( char parentesis)
	{
		char contrario;

		if (parentesis == ']')
			contrario = '[';
		else
			contrario = '(';

		return contrario;
	}

	private static boolean esOperador(char caracter)
	{
		boolean es = false;

		if (
				caracter == '+' ||
				caracter == '-' ||
				caracter == '*' ||
				caracter == '/' 

				)
			es = true;

		return es;

	}

	private static Stack<Character> volverPila (String exp)
	{
		exp = exp.replace(" ", "");

		System.out.println(exp);
		Stack<Character> pila = new Stack<Character>();

		for (int i = 0; i< exp.length(); i++)
		{
			char x = exp.charAt(i);
			pila.push(x);
		}

		return pila;
	}

	public static Stack<Character> ordenarPila(Stack<Character> pila)
	{
		Stack<Character> ordenada = new Stack<Character>();
		Stack<Character> aux1 = new Stack<Character>();
		Stack<Character> aux2 = new Stack<Character>();
		Stack<Character> aux3 = new Stack<Character>();
		
		while (!pila.isEmpty())
		{
			char actual = pila.pop();
			
			aux1.push(actual);
			aux2.push(actual);
			aux3.push(actual);
		}
		
		while (!aux1.isEmpty())
		{
			char actual = aux1.pop();
			if (!esParentesis(actual) && !esOperador(actual))
			{
				ordenada.push(actual);
			}
		}
	
		while (!aux2.isEmpty())
		{
			char actual = aux2.pop();
			if (esOperador(actual))
			{
				ordenada.push(actual);
			}
		}
		
		while (!aux3.isEmpty())
		{
			char actual = aux3.pop();
			if (esParentesis(actual))
			{
				ordenada.push(actual);
			}
		}

		return ordenada;
	}

	public static void main(String[] args) {

		System.out.println(expresionBienFormada("[(0+4)*4]"));

		
		Stack<Character> pila = volverPila("[ (5+6+32*(7-1)) * (3+8)] ");
		
		System.out.println(ordenarPila(pila));


	}


}
