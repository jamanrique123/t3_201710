package model.data_structures;

public class Queue<T> implements IQueue<T> {

	private int size = 0;
	
	private ListaEncadenada<T> cola = new ListaEncadenada<T>();
	
	@Override
	public void enqueue(T item)
	{
		// TODO Auto-generated method stub
		size++;
		cola.agregarElementoFinal(item);
	}

	@Override
	public T dequeue()
	{
		// TODO Auto-generated method stub
		
		T ultimo = cola.eliminarPrimero();
		size--;
		
		return ultimo;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto0-generated method stub
		return size == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}
	
	public String toString()
	{
		String respuesta = cola.toString();
		
		return respuesta;
	}

}
