package model.data_structures;

import com.sun.xml.internal.bind.v2.runtime.RuntimeUtil.ToStringAdapter;

public class Stack<T>implements IPila<T> {
	
	private int size;
	
	private ListaEncadenada<T> pila = new ListaEncadenada<T>();
	
	public Stack()
	{
		size = 0;
	}
	
	@Override
	public void push(T item) {
		// TODO Auto-generated method stub
		pila.agregarAlInicio(item);
		size++;
	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		
		T popeado = pila.eliminarPrimero();
		
		size--;
		return popeado;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	public String toString()
	{
		String respuesta = pila.toString();
		
		return respuesta;
	}
	
}
