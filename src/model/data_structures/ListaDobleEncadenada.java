package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoblementeEncadenado<T> primero;

	private NodoDoblementeEncadenado<T> ultimo;
	
	//representa la posicion actual de la lista, para desplazarse por ella
	private int nodoActual = 0;

	private int numeroDeNodos = 0;

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub

		Iterator<T> iter =  new Iterator<T>() {

			int actualLocal = 0;

			@Override
			public boolean hasNext()
			{
				// TODo Auto-generated method stub

				return actualLocal < numeroDeNodos;
			}

			@Override
			public T next() {
				// TODo Auto-generated method stub

				actualLocal++;
				return darElemento(actualLocal-1);
			}
			
			@Override
			public void remove()
			{
				
			}
		};

		return iter;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		
		if (primero == null) //si la lista est� vac�a, se inicializa
		{
			primero = new NodoDoblementeEncadenado<T>(null, null, elem);
			ultimo = primero;
			
			numeroDeNodos = 1;
		}
		else
		{			
			//Creo un nodo que el anterior sea el �ltimo, el siguiente nullo y su elemento sea
			//el que entr� por par�metro
			
			NodoDoblementeEncadenado<T> nodoFinal = new NodoDoblementeEncadenado<T>(ultimo, null, elem);
			
			//Cambio al �ltimo su siguiente, para que sea el nuevo
			ultimo.cambiarSiguiente(nodoFinal);
			
			//Ahora el nodo nuevo es el �ltimo
			ultimo = nodoFinal;
			
			numeroDeNodos ++;
		}

	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub

		int i =0;
		T elem = null;

		if (primero!= null)
		{
			NodoDoblementeEncadenado<T> actual = null;
			
			elem = primero.darElemento();
			actual = primero;

			while (i < pos)
			{
				actual = actual.darSiguiente();

				i++;
			}
			
			elem = actual.darElemento();
		}


		return elem;
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return numeroDeNodos;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return darElemento(nodoActual);
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		
		boolean puedoAvanzar = true;
		
		if(nodoActual + 1 == numeroDeNodos) 
		//Si el siguiente al actual es igual el n�mero de nodos, no existe dicho elemento, entonces, no puedo avanzar a dicha posicion
		{
			puedoAvanzar = false;
		}
		else
		{
			nodoActual++;
		}
		
		return puedoAvanzar;
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		
		boolean puedoRetroceder = true;
		
		if (nodoActual-1 <0)
		//Si el anterior a nodo actual es negativo, entonces no puedo retroceder
		{
			puedoRetroceder = false;
		}
		else
		{
			nodoActual--;
		}
		
		return puedoRetroceder;
	}

	public String toString()
	{
		String respuesta = "[";
		
		Iterator<T> iter = iterator();
		
		while (iter.hasNext())
		{
			respuesta = respuesta + iter.next().toString();
			
			if(iter.hasNext())
				respuesta = respuesta +", ";
		}
		
		
		return respuesta + "]";
	}
}

