package model.data_structures;

import java.util.Iterator;




public class ListaEncadenada<T> implements Iterable<T>, ILista<T> {

	private NodoEncadenado<T> primero;
	
	private NodoEncadenado<T> ultimo;
	
	private int posicionActual;
	
	private int tamanioLista;


	public ListaEncadenada()
	{
		primero = null;
		tamanioLista = 0;
		ultimo = null;
		posicionActual = 0;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteradorListaEncadenada<T>(this);		
	}


	@Override
	public void agregarElementoFinal(T elemento) {
		// TODO Auto-generated method stub
		if(primero == null)
		{
			primero = new NodoEncadenado<T>(elemento);
			ultimo = primero;
		}
		else
		{
			ultimo = ultimo.agregarElementoFinal(elemento);
		}
		tamanioLista++;
	}

	@Override
	public T darElemento(int posicion) {
		// TODO Auto-generated method stub
		if(posicion < 0)
		{
			throw new IndexOutOfBoundsException("posicion debe ser mayor o igual a 0");
		}
		if(primero == null)
		{
			throw new IndexOutOfBoundsException("No hay ning�n elemento en la lista.");
		}
		return primero.darElementoEn(posicion);

	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return tamanioLista;
	}
	public class IteradorListaEncadenada<T> implements Iterator<T>
	{
		
		private NodoEncadenado<T> actual;
		
		public IteradorListaEncadenada(ListaEncadenada<T> listaEncadenada)
		{
			actual = listaEncadenada.primero;
		}
		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return actual != null;
		}

		@Override
		public T next() {
			// TODO Auto-generated method stub
			NodoEncadenado<T> nodoARetornar = actual;
			actual = actual.darSiguiente();
			return nodoARetornar.darElemento();
		}
		
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return darElemento(posicionActual);
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if(posicionActual >0)
		{
			posicionActual--;
			return true;
		}
		return false;
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if(tamanioLista-1 > posicionActual)
		{
			posicionActual++;
			return true;
		}
		return false;
	}
	public String toString()
	{
		String respuesta = "[";
		Iterator<T> iterador = iterator();
		while(iterador.hasNext())
		{
			respuesta = respuesta + iterador.next().toString();
			if(iterador.hasNext())
			{
				respuesta = respuesta+", ";
			}
		}
		return respuesta + "]";
		
	}
	
	public T eliminarPrimero()
	{
		T eliminado = null;
		
		if (tamanioLista != 0)
		{			
			NodoEncadenado<T> nodoEliminado = primero;
			
			primero = nodoEliminado.darSiguiente();
			
			//Elemento eliminado del nodo
			
			eliminado = nodoEliminado.darElemento();				
		}
		
		return eliminado;
	}
	


	public void agregarAlInicio(T elem)
	{
		if (elem != null)
		{
			NodoEncadenado<T> nodoNuevo = new NodoEncadenado<T>(elem);
			
			nodoNuevo.cambiarSiguiente(primero);
			
			primero = nodoNuevo;
			
			tamanioLista ++;
		}
		else
		{
			System.out.println("Se intent� agregar un elemento nulo, por lo tanto no se agreg�");
		}
	}
}
