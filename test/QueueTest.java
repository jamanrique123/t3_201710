import static org.junit.Assert.*;

import java.util.Deque;

import model.data_structures.ListaDobleEncadenada;
import model.data_structures.Queue;

import org.junit.Test;


public class QueueTest {

	/**
	 * Lista a evaluar
	 */
	private Queue<String> cola;
	
	/**
	 * Crea una lista y le asigna pocos valores arbitrarios
	 */
	private void ambiente1()
	{
		cola = new Queue<String>();
		
		cola.enqueue("Holigrama");
		cola.enqueue("Chaograma");
		cola.enqueue("Writable");
		cola.enqueue("Neon");
		cola.enqueue("Kepler");
	}
	
	/**
	 * Crea una lista y le asigna una cantidad espec�fica de elementos dados por par�metro<br>
	 * Se usa para probar con grandes cantidades de elementos
	 * @param numElementos cantidad de elementos a agregar
	 */
	private void ambiente2(int numElementos)
	{
		cola = new Queue<String>();
		
		//Se agrega desde elem: 0 hasta elem: n, osea que
		//El tama�o de la lista es numElementos
		//El �ndice m�ximo que funciona darELemento es numElementos - 1
		for (int i = 0; i < numElementos; i++)
		{
			cola.enqueue("elem: " +i);
		}
	}

	//hacer dequeue un numero n de veces
	private void dequeueVariasVeces(int n)
	{
		for(int i = 0; i < n; i++)
			System.out.println("Saqu� a: " + cola.dequeue());
	}
	
	@Test
	public void test() {
		
		//Pocos valores arbitrarios
		ambiente1();
		
		System.out.println(cola + "\n");
		
		assertEquals("String incorecto", "Holigrama", cola.dequeue());
		
		System.out.println(cola.dequeue());
		System.out.println(cola.dequeue());
		
		assertEquals("String incorecto", "Neon", cola.dequeue());
		
		assertEquals("Cantidad err�nea", 1, cola.size());
	
		cola.dequeue();
		
		assertEquals("Cantidad err�nea", 0, cola.size());
		
		assertTrue(cola.isEmpty());
		
		//Cantidad un poco mayor de valores
		ambiente2(100);
		
		//hago 20 veces, porque quiero llegar al 19, es decir, la posici�n 0 + las otras 19
		dequeueVariasVeces(20);
		
		assertEquals("String incorecto", "elem: 20", cola.dequeue());
		
		dequeueVariasVeces(69);
		
		assertEquals("String incorecto", "elem: 90", cola.dequeue());
		
		dequeueVariasVeces(8);
		
		assertEquals("String incorecto", "elem: 99", cola.dequeue());
		
		//no hay nada porque la elemento 99 (el ultimo) le hice dequeue()
		assertEquals("Cantidad err�nea", 0, cola.size());
		
		//Esto no deber�a poder hacerse
		try
		{
			cola.dequeue();
			fail();
		}
		catch (Exception e)
		{
			System.out.println("bien!");
		}
		
		assertTrue(cola.isEmpty());
		
	}
	

}

