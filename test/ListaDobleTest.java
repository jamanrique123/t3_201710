

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.ListaDobleEncadenada;

/**
 * Test's de la clase ListaDobleEncadenada
 * @author Juli�nManrique
 *
 */

public class ListaDobleTest {

	/**
	 * Lista a evaluar
	 */
	private ListaDobleEncadenada<String> lista;
	
	/**
	 * Crea una lista y le asigna pocos valores arbitrarios
	 */
	private void ambiente1()
	{
		lista = new ListaDobleEncadenada<String>();
		
		lista.agregarElementoFinal("Holigrama");
		lista.agregarElementoFinal("Chaograma");
		lista.agregarElementoFinal("Writable");
		lista.agregarElementoFinal("Neon");
		lista.agregarElementoFinal("Kepler");
	}
	
	/**
	 * Crea una lista y le asigna una cantidad espec�fica de elementos dados por par�metro<br>
	 * Se usa para probar con grandes cantidades de elementos
	 * @param numElementos cantidad de elementos a agregar
	 */
	private void ambiente2(int numElementos)
	{
		lista = new ListaDobleEncadenada<String>();
		
		//Se agrega desde elem: 0 hasta elem: n, osea que
		//El tama�o de la lista es numElementos
		//El �ndice m�ximo que funciona darELemento es numElementos - 1
		for (int i = 0; i < numElementos; i++)
		{
			lista.agregarElementoFinal("elem: " +i);
		}
	}

	@Test
	public void test() {
		
		//Pocos valores arbitrarios
		ambiente1();
		
		System.out.println(lista + "\n");
		
		assertEquals("String incorecto", "Holigrama", lista.darElemento(0));
		assertEquals("String incorecto", "Neon", lista.darElemento(3));
		
		assertEquals("Cantidad err�nea", 5, lista.darNumeroElementos());
		
		assertEquals("Posici�n equivocada", "elem: 0", lista.darElementoPosicionActual());
		
		//Cantidad un poco mayor de valores
		ambiente2(100);
		
		System.out.println(lista);
		
		assertEquals("String incorecto", "elem: 20", lista.darElemento(20));
		assertEquals("String incorecto", "elem: 90", lista.darElemento(90));
		assertEquals("String incorecto", "elem: 99", lista.darElemento(99));
		
		assertEquals("Cantidad err�nea", 100, lista.darNumeroElementos());
		
		assertEquals("Posici�n equivocada", "elem: 0", lista.darElementoPosicionActual());
		
		//Vamos a probar los m�todos de desplazamiento
		int i =0;
		
		while (i<20)//con este while avanzo 20 elementos/posiciones
		{
			lista.avanzarSiguientePosicion();
			i++;
		}
		
		assertEquals("Posici�n equivocada", "elem: 20", lista.darElementoPosicionActual());
		
		i=0;
		
		while (i<5)// con este, me devuelvo 5
		{
			lista.retrocederPosicionAnterior();
			i++;
		}
		
		assertEquals("Posici�n equivocada", "elem: 15", lista.darElementoPosicionActual());
	}

}

